section .data
%define x 10
newl: db x,0
numbers: db "0123456789", 0
negative: db "-", 0
charbuf:db "",0
tab1: dw x, 0
tab2: dw 32, 0
tab3: dw 9, 0
a0: dq 00000048; ascii 0
a9: dq 00000057; ascii 9
bs: db 8, 0; byte size
ec: db 60, 0 ;exit code
ppc: db 1, 0;print code and one
rc: dq 00000000;read code
dns: dq x, 0;decimal number system

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, ec
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    	.loop:
		cmp byte [rdi+rax],0
		je .end
		inc rax
		jmp .loop

	.end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	mov rdx, rax
    	mov rsi, rdi
	xor rax, rax
    	mov al, [ppc]
    	mov rdi, [ppc]
   	syscall
	   pop rdi
    ret

; Принимает код символа и выводит его в stdout
print_char:
	xor rax, rax
	mov word [charbuf], di
	mov rax, [ppc]
	mov rdi, [ppc]
	mov rsi, charbuf
	mov rdx, [ppc]
	syscall
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, newl
	call print_char
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	xor rcx, rcx
	mov rax, rdi
	mov rbx, [dns]
	.loop:
		xor rdx, rdx
		inc rcx
		div rbx
		push rdx
		test rax, rax
		jnz .loop
	xor rbx, rbx
	.loop_print:
		dec rcx
		pop rdx
		lea rsi, [numbers+rdx]
		mov rax, [ppc]
		mov rdi, [ppc]
		mov rdx, [ppc]
		push rcx
		syscall
		pop rcx
		test rcx, rcx
		jnz .loop_print
    	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	push rdi
	mov rax, rdi
	test rax, rax
	js .neg
	call print_uint
	pop rdi
    ret
	.neg:
		mov rbx, rdi
		mov rax, [ppc]
		mov rdi, [ppc]
		mov rsi, negative
		mov rdx, [ppc]
		syscall
		mov rax, rbx
		neg rax
		mov rdi, rax
		call print_uint
	pop rdi	
	ret
		

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	push rdi
	call string_length
	push rax
	mov rdx, rdi
	mov rdi, rsi
	call string_length
	pop rcx
	push rdx
	cmp rax, rcx
	jne .unsuccessful
	test rax, rax
	je .successful
	pop rdx
	mov rsi, rdx
	mov rdx, rax
	xor rcx, rcx
	.cycle:
		push rdx
		mov  dl, [rdi+rcx]
		mov  al, [rsi+rcx]
		cmp rax, rdx
		jne .unsuccessful
		inc rcx
		pop rdx
		cmp rdx, rcx
		jne .cycle
	mov rax, [ppc]
	pop rdi
	ret
	.unsuccessful:
		pop rdx
		xor rax, rax
		pop rdi
		ret
	.successful:
		pop rdx
		mov rax, [ppc]
		pop rdi
		ret
		

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	mov rax, [rc]
	mov rdi, [rc]
	mov rsi, charbuf
	mov rdx, [ppc]
	syscall
	test al, al
	je .zer
	mov al, [charbuf]
    ret 
	.zer:
		xor rax, rax
		ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push rsi
	xor rcx, rcx
	push rcx
	.check_space:
		push rdi
		call read_char
		test rax, rax
		je .null
		pop rdi
		mov al, [rsi]
		cmp al, [tab1]
		je .check_space
		cmp al, [tab2]
		je .check_space
		cmp al, [tab3]
		je .check_space
		mov byte [rdi], al
	.loop_read:
		pop rcx
		pop rax
		cmp rax, rcx
		je .err
		push rax
		push rcx
		push rdi
		call read_char
		mov rdx, rax
		pop rdi
		pop rcx
		mov al, [rsi]
		inc rcx
		mov byte [rdi+rcx],al
		push rcx
		mov rax, rdx
		cmp al, [tab1]
		je .end
		cmp al, [tab2]
		je .end
		cmp al, [tab3]
		je .end
		test rax, rax
		jnz .loop_read
	.end:
		pop rcx 
		mov byte [rdi+rcx], 0
		pop rdx
		mov rdx, rcx
		mov rax, rdi
		ret
	.null:
		pop rax
		pop rax
		pop rax
		xor rax, rax
		xor rdx, rdx
    	ret
	.err:
		xor rax, rax
		xor rdx, rdx
    	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rcx, rcx
	mov al, [rdi+rcx]
	cmp al, [a0]
	jl .no_num
	cmp al, [a9]
	jg .no_num
	sub rax, [a0]
	push rax
	inc rcx
	.loop_num:
		mov al, [rdi+rcx]
		cmp rax, [a0]
		jl .end_num
		cmp al, [a9]
		jg .end_num
		sub rax, [a0]
		push rax
		inc rcx
		jmp .loop_num
	.no_num:
		xor rax, rax
		xor rdx, rdx
		ret
	.end_num:
		xor rdi, rdi
		mov rdx, rcx
	.end_loop:
		mov rax, [ppc]
		mov rsi, rdx
		sub rsi, rcx
	.sqr:
		test rsi, rsi
		je .next
		imul rax, [dns]
		dec rsi
		jmp .sqr
	.next:
		pop rsi
		imul rax, rsi
		add rdi, rax
		dec rcx
		test rcx, rcx
		jne .end_loop
	mov rax, rdi
    	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
    	mov al, [rdi]
	cmp al, [negative]
	je .negat
	cmp al, [a0]
	jl .no_num
	cmp al, [a9]
	jg .no_num
	sub rax, [a0]
	call parse_uint
	ret
	.negat:
		inc rdi
		call parse_uint
		inc rdx
		neg rax
		ret
	.no_num:
		xor rax, rax
		xor rdx, rdx
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi
	call string_length
	cmp rax, rdx
	jg .err_size
	mov rcx, rax
	xor rdx, rdx
	.loop_copy:
		mov rax, [rdi+rdx]
  		mov qword [rsi+rdx], rax
		xor rax, rax
		cmp cl, [bs]
		jg .mov_cycle
		jmp .next_copy
	.mov_cycle:
		sub rcx, [bs]
		add dl, [bs]
		jmp .loop_copy
	.next_copy:
		mov rdi, rsi
		call string_length
		pop rdi
		ret
	.err_size:
		xor rax, rax
		pop rdi
		ret
